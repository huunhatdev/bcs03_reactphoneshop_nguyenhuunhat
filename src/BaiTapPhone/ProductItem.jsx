import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { item, handleDetail, handleAddCart } = this.props;
    return (
      <div className="card col-4 " style={{ width: "18rem" }}>
        <img
          className="card-img-top img-fluid"
          //   style={{ height: "20rem" }}
          src={item.hinhAnh}
          alt="hình ảnh điện thoại"
        />
        <div className="card-body">
          <h5 className="card-title text-left">{item.tenSP}</h5>

          <div className="row">
            <button
              className="btn btn-success m-1"
              onClick={() => handleDetail(item.maSP)}
            >
              Xem chi tiết
            </button>
            <button
              className="btn btn-danger m-1"
              onClick={() => handleAddCart(item)}
            >
              Thêm vào giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}
