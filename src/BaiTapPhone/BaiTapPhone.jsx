import React, { Component } from "react";
import ProducList from "./ProducList";
import data from "./data.json";
import Detail from "./Detail";
import ModalComponent from "./ModalComponent";

export default class BaiTapPhone extends Component {
  state = {
    data: data,
    cart: [],
    detail: 0,
  };

  handleDetail = (maSP) => {
    this.setState({
      detail: maSP,
    });
  };

  handleAddCart = (product) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((e) => {
      return e.maSP === product.maSP;
    });
    if (index === -1) {
      cloneCart.push({ ...product, quantity: 1 });
    } else {
      cloneCart[index].quantity += 1;
    }

    this.setState({
      cart: cloneCart,
    });
  };
  handleSubCart = (i) => {
    let cloneCart = [...this.state.cart];
    let quant = cloneCart[i].quantity;
    if (quant > 1) {
      cloneCart[i].quantity -= 1;
    } else {
      cloneCart.splice(i, 1);
    }

    this.setState({
      cart: cloneCart,
    });
  };

  handleRemove = (i) => {
    let cloneCart = [...this.state.cart];
    cloneCart.splice(i, 1);
    this.setState({
      cart: cloneCart,
    });
  };

  countProductInCart = () => {
    return this.state.cart.length;
  };
  dataDetail = () => {
    return this.state.data.find((e) => e.maSP === this.state.detail);
  };

  render() {
    return (
      <div>
        <ProducList
          data={this.state.data}
          handleDetail={this.handleDetail}
          handleAddCart={this.handleAddCart}
          countCart={this.state.cart.length}
        />
        {this.state.detail !== 0 && <Detail item={this.dataDetail()} />}
        <ModalComponent
          data={this.state.cart}
          handleRemove={this.handleRemove}
          addProduct={this.handleAddCart}
          subProduct={this.handleSubCart}
        />
      </div>
    );
  }
}
