import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProducList extends Component {
  render() {
    let { data, handleDetail, handleAddCart, countCart } = this.props;

    return (
      <div className="container">
        <h2 className="text-success">Bài tập giỏ hàng</h2>
        <div className="row justify-content-end">
          <span
            className="btn btn-danger mb-3"
            data-toggle="modal"
            data-target="#modelId"
          >
            Giỏ hàng ({countCart})
          </span>
        </div>
        <div className="product row">
          {data.map((e, i) => {
            return (
              <ProductItem
                item={e}
                key={i}
                handleDetail={handleDetail}
                handleAddCart={handleAddCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
