import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    let { item } = this.props;
    console.log(item);
    return (
      <div className="container mt-4">
        <hr />
        <h2 className="text-warning">Xem chi tiết</h2>
        <div className="row">
          <div className="col-4">
            <img
              src={item.hinhAnh}
              alt="hinh ảnh điện thạoi"
              className="img-fluid"
            />
          </div>
          <div className="col-8">
            <table class="table table-hover text-left">
              <thead>
                <tr>
                  <th colSpan={2}>Thông số kỹ thuật</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Màn hình</td>
                  <td>{item.manHinh}</td>
                </tr>
                <tr>
                  <td>Hệ điều hành</td>
                  <td>{item.heDieuHanh}</td>
                </tr>
                <tr>
                  <td>Camera Trước</td>
                  <td>{item.cameraTruoc}</td>
                </tr>
                <tr>
                  <td>Camera Sau</td>
                  <td>{item.cameraSau}</td>
                </tr>
                <tr>
                  <td>Ram</td>
                  <td>{item.ram}</td>
                </tr>
                <tr>
                  <td>Rom</td>
                  <td>{item.rom}</td>
                </tr>
                <tr>
                  <td>Giá bán</td>
                  <td className="text-info">{item.giaBan} vnđ</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
